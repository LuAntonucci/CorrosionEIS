## Tags utilizados en el archivo "interface.ui"  
  

### Barra de menú 

- **menu_Archivo**: menú principal, al desplegarlo aparecen las siguientes opciones:
    - **actionAbrir**: le permite al usuario abrir archivos tipo _.csv_.
    - **actionImportar**: opción para importar datos desde archivos _.dat_ (inicialmente generados por potenciostatos de la marca GAMRY).
    - **actionGuardar**: le permite al usuario almacenar los datos en un formato estándar. (Verificar y completar)
    - **actionExit**

### Sección de tabuladores

- **tab_modelo**: título del tabulador para la generación del modelo.
  - **label_cdc**: etiqueta asociada al cuadro de texto para el circuito equivalente. 
  - **plainTextEdit_cdc**: cuadro de texto plano, donde el usuario ingresa el circuito equivalente a ajustar.
  - **label_parámetros**: etiqueta bajo la cual aparecen los parámetros del circuito planteado en plainTextEdit_cdc. 
  - **label_fijar**: etiqueta bajo la cual aparecen los cuadros de selección para fijar los valores de los parámetros durante el ajuste.
  - **label_limiteinferior** y **label_limitesuperior**: etiquetas para los cuadrados de texto plano, en los cuales el usuario informará los valores límites inferiores y superiores, respectivamente, que considere adecuados para el circuito bajo estudio.
  - **label_ajuste**: etiqueta bajo la cual se presentan los valores ajustados por el software.  
  
- **tab_ajuste**: título del tabulador para la elección del método de ajuste y la ponderación a utilizar.
  - **groupBox_metodo**: contenedor para agrupar elementos tipo check-box relativos al método de ajuste de los datos; las opciones son:
    - **radioButton_levenberg**: si está seleccionada, el ajuste se realiza por el método Levenberg-Marquardt.
    - **radioButton_simplex**: si está seleccionada, el ajuste se realiza por el método Simplex.
  - **groupBox_ponderacion**: contenedor para agrupar elementos tipo check-box relativos a la ponderacion deseada por el usuario para el ajuste de los datos; las opciones son:
    - **radioButton_unitario**: los factores de ponderación son iguales a la unidad.
    - **radioButton_proporcional**: en cada frecuencia, el factor de ponderación es igual a la parte real o imaginaria a dicha frecuencia, según que factor se considere.  
    - **radioButton_moduloZ**: en cada punto, los factores de ponderación son iguales al módulo de impedancia a la frecuencia de ese punto.  
  
- **tab_simulación**: título del tabulador para la simulación de espectros en un rango de frecuencia seleccionado por el usuario, utilizando los parámetros ajustados por el software.
  - **groupBox_rangofrecuencias**: contenedor para agrupar elementos relativos al rango de frecuencia deseado por el usuario para la simulación de espectros de impedancia; agrupa los siguientes elementos:
     - **label_freqincial** y **label_freqfinal**: etiquetas para los cuadrados de texto plano, en los cuales el usuario informará los valores inicial y final, respectivamente, que considere adecuados para la simulación del espectro de impedancia para el circuito bajo estudio.
     - **plainTextEdit_freqinicial** y **plainTextEdit_freqifinal**: cuadros de texto plano, donde el usuario ingresa los valores en Hz de las frecuencias inicial y final, respectivamente, que desea utilizar durante la simulación.
     
### Sección de gráficos

- **verticalLayout_bode**: layout reservado, en la parte superior derecha de la pantalla, para la presentación del diagrama de Bode de los datos importados por el usuario.
- **verticalLayout_nyquist**: layout reservado, en la parte inferior derecha de la pantalla, para la presentación del diagrama de Nyquist de los datos importados por el usuario.

  
  
  

